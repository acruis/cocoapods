//
//  BubblePhysics.swift
//  BubblePhysics
//
//  Created by Quoc Dat Nguyen on 27/2/16.
//  Copyright © 2016 Quoc Dat Nguyen. All rights reserved.
//

import Darwin

public class BubblePhysics {
    public static func angleBetweenPoints(fromX: Double,
                                          fromY: Double,
                                            toX: Double,
                                            toY: Double) -> Double {
        let displaceX = toX - fromX
        let displaceY = toY - fromY
        
        return atan(displaceX/displaceY)
    }
    
    public static func distanceBetweenPoints(fromX: Double,
                                             fromY: Double,
                                               toX: Double,
                                               toY: Double) -> Double {
        let xDist = abs(fromX - toX)
        let yDist = abs(fromY - toY)
        return sqrt(pow(xDist, 2) + pow(yDist, 2))
    }
    
    public static func circularHitboxCollision(fromX: Double,
                                               fromY: Double,
                                                 toX: Double,
                                                 toY: Double,
                                          fromRadius: Double,
                                            toRadius: Double) -> Bool {
        return distanceBetweenPoints(fromX, fromY: fromY, toX: toX, toY: toY) <= fromRadius + toRadius
    }
}