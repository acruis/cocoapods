Pod::Spec.new do |s|

  s.name         = "BubblePhysics"
  s.version      = "0.0.4"
  s.summary      = "Bare-bones physics for circular objects."
  s.homepage     = "https://acruis@bitbucket.org/acruis/cocoapods.git"
  s.license      = "MIT"
  s.author       = "acruis"
  s.source       = { :git => "https://acruis@bitbucket.org/acruis/cocoapods.git", :tag => "0.0.4" }
  s.platform     = :ios, "8.0"
  s.source_files  = "BubblePhysics"
  s.exclude_files = "BubblePhysics/Exclude"

end
